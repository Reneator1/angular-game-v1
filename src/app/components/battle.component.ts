import {Component, Input, OnInit} from '@angular/core';

import {Hero} from '../classes/hero';
import {HeroService} from '../services/hero.service';
import {Monster} from '../classes/monster';
import {MonsterNameService} from '../services/monstername.service';
import {MonsterService} from '../services/monster.service';
import {AppComponent} from './app.component';
import {Battle} from '../classes/battle';


@Component({
  selector: 'pve-battle',
  templateUrl: './battle.component.html',
  providers: [HeroService, MonsterService, MonsterNameService],

})


export class BattleComponent implements OnInit {
  player: Hero;
  monster: Monster;

  @Input() battle: Battle;


  constructor() {

  }

  attack(): void {
    // this.playAudio('../assets/sounds/pick_a_name.ogg');
    this.monster.damageCharacter(this.player);
    this.player.damageCharacter(this.monster);
    if (!this.player.alive) {
      this.gameOver();
    }
    if (!this.monster.alive) {
      this.rewardPlayer();
      this.battle.active = false;

    }
  }

  playAudio(source): void {
    const audio = new Audio();
    audio.src = source;
    audio.load();
    audio.play();
  }

  gameOver(): void {
// show Overlay/dialogue to inform the player, he is dead, and he can start anew
  }

  rewardPlayer(): void {
    this.player.gainGold(this.monster.gold);
    this.player.gainExp(this.monster.experience);
  }

  ngOnInit(): void {

    this.player = this.battle.player;
    this.monster = this.battle.enemy;
  }


}
