import {Component, OnChanges, OnInit, SimpleChanges} from '@angular/core';

import {Hero} from '../classes/hero';
import {HeroService} from '../services/hero.service';
import {MonsterService} from '../services/monster.service';
import {MonsterNameService} from '../services/monstername.service';
import {Monster} from '../classes/monster';
import {Battle} from '../classes/battle';
import {HeroLevelService} from '../services/herolevel.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [HeroService, MonsterService, MonsterNameService, HeroLevelService]
})
export class AppComponent implements OnInit, OnChanges {
  title = 'Hero Battle';
  player: Hero;
  randomNumber: number;
  monster: Monster;
  battle: Battle;

  constructor(private heroService: HeroService, private monsterNameService: MonsterNameService,
              private monsterService: MonsterService, private heroLevelService: HeroLevelService) {
  }

  generateNumber(): void {
    this.randomNumber = this.monsterNameService.getRandomNumber();
  }

  generateNumberMonster(): void {
    this.randomNumber = this.monsterService.getRandomNumber();
  }

  generateMonster(): void {
    let genName: string;
    genName = this.monsterNameService.getRandomMonsterName();
    this.monster = this.monsterService.getRandomMonster(genName);
  }

  generateHero(): void {
    this.player = this.heroService.getHero();
    this.player.heroLevel = this.heroLevelService.generateHeroLevel();

  }


  ngOnInit(): void {
    this.generateHero();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.saveState();
  }

  startBattle(): void {
    this.generateMonster();
    this.battle = new Battle(this.player, this.monster);
    this.battle.active = true;
  }


  saveState(): void {
  }


}
