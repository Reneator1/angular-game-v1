import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';

import {AppComponent} from './app.component';
import {HeroDetailComponent} from './hero-detail.component';
import {BattleComponent} from './battle.component';
import {RouterModule} from '@angular/router';
import {FileUploadModule} from 'angular-file-upload';
import {ShopComponent} from './shop.component';


@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot([

      {
        path: 'detail/:id',
        component: HeroDetailComponent
      },
      {
        path: 'battle',
        component: BattleComponent
      },
      {
        path: 'shop',
        component: ShopComponent
      },
    ]),
  ],
  declarations: [
    AppComponent,
    HeroDetailComponent,
    BattleComponent,
    ShopComponent,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
