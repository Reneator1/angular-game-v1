import {Component, OnChanges, OnInit, SimpleChanges} from '@angular/core';

import {Hero} from '../classes/hero';
import {Battle} from '../classes/battle';
import {ShopService} from '../services/shop.service';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.css'],
  providers: [ShopService]
})
export class ShopComponent {
  title = 'Shop';
  player: Hero;
  battle: Battle;

  constructor(private shopService: ShopService) {
  }

}
