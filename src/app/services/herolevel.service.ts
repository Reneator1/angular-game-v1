import {Injectable} from '@angular/core';

import {Hero} from '../classes/hero';
import {HeroLevel} from '../classes/herolevel';
import {LEVEL_STEPPINGS} from '../mockdata/mock-level-steppings';

@Injectable()
export class HeroLevelService {

  generateHeroLevel(): HeroLevel {
    return new HeroLevel(1, 0, LEVEL_STEPPINGS);
  }


}
