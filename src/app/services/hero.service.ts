import {Injectable} from '@angular/core';

import {Hero} from '../classes/hero';
import {HERO} from '../mockdata/mock-heroes';

@Injectable()
export class HeroService {

  getHero(): Hero {
    return this.newHero();
  }

  newHero(): Hero {
    return HERO;
  }

}
