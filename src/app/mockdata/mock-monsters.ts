import {Monster} from '../classes/monster';

export const MONSTERS: Monster[] = [
  new Monster('', 500, 10, true, 30, 'WereWolf', 'Water', 'This monster fears water, although it was born in it, molded by it!', 100),
  new Monster('', 500, 10, true, 45, 'Woof', 'Earth', 'Born from Furfags making love. A scientific impossibility!', 100),
  new Monster('', 500, 10, true, 50, 'Firedispenser', 'Fire', 'Born from the raging fires of the Underworld.', 100),
];
