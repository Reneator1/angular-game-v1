import {Monster} from '../classes/monster';

export const MONSTER_NAMES: string[] = [
  'Hugo', 'Willi', 'Charlie', 'Homer', 'Phoebe', 'Charles', 'William', 'Klaus'
];
