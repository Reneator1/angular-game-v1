import {Hero} from './hero';
import {Monster} from './monster';

export class Battle {
  enemy: Monster;
  player: Hero;
  // loot: Loot;
  active = false;

  constructor(player: Hero, enemy: Monster) {
    this.player = player;
    this.enemy = enemy;
  }
}
